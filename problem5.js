// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
export default function oldCars(cars_inventory, yearOfManufacture){
    const old_cars = [];
    for (let year_index=0; year_index < yearOfManufacture.length; year_index++){
        if (yearOfManufacture[year_index] < 2000){
            for (let cars_index = 0; cars_index < cars_inventory.length; cars_index++){
                if(cars_inventory[cars_index].car_year === yearOfManufacture[year_index] && !old_cars.includes(cars_inventory[cars_index]) ){
                    old_cars.push(cars_inventory[cars_index]);
                }
            }
        }
    }
    return old_cars;
}

