// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
export default function filterCars(cars_inventory,...carMake){
    const filtered_cars=[]
    for(let cars_index=0;cars_index<cars_inventory.length;cars_index++){
        if(carMake.includes(cars_inventory[cars_index].car_make)){
            filtered_cars.push(cars_inventory[cars_index])
        }
    }
    if(filtered_cars.length>0){
        return filtered_cars;}
    else{
        return "Required car manufacturer not in inventory";
    }
}