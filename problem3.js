// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
export default function sortedCarModels(cars_inventory){
    let car_models = [];
    for (let cars_index = 0; cars_index < cars_inventory.length; cars_index++) {
        car_models.push(cars_inventory[cars_index].car_model);
    }
    let swapped;
    do {
        swapped = false;
        for (let cars_index = 0; cars_index < car_models.length - 1; cars_index++) {
            if (car_models[cars_index].toLowerCase() > car_models[cars_index + 1].toLowerCase()) {
                let temperorayValue = car_models[cars_index];
                car_models[cars_index] = car_models[cars_index+1];
                car_models[cars_index+1] = temperorayValue;
                swapped = true;
            }
        }
    } while (swapped);
    return car_models;
}
