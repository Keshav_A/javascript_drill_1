// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
export default function carDetailFunction(cars_inventory,car_id){
    for (let cars_index =0 ;cars_index<cars_inventory.length;cars_index++){
        if (cars_inventory[cars_index].id===car_id){
            return cars_inventory[cars_index]
        }
    }
}