// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
import inventory from "../data.js"
import  sortedCarModels  from "../problem3.js"
const sorted_inventory = sortedCarModels(inventory);
console.log(sorted_inventory);