// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
import inventory from "../data.js"
import  carDetailFunction  from "../problem1.js"
let id = 33;
const car_Detail = carDetailFunction(inventory,id);
console.log("Car "+ id + " is a " + car_Detail.car_year + " " + car_Detail.car_make + " " + car_Detail.car_model);