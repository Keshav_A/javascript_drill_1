// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"
import inventory from "../data.js"
import  latestCarDetail  from "../problem2.js"
const last_Car=latestCarDetail(inventory);
console.log("Last car is a "+last_Car.car_make+" "+last_Car.car_model);