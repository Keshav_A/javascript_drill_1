// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
import yearOfManufacture from "./testProblem_4.js";
import inventory from "../data.js";
import oldCars from "../problem5.js";
let old_cars = oldCars(inventory, yearOfManufacture)
console.log(old_cars)
console.log("The number of cars older than the year 2000 are: " + old_cars.length)