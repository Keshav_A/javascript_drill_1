// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
export default function manufacturedYear(cars_inventory){
    const manufactured_Year=[];
    for (let car_index=0;car_index < cars_inventory.length; car_index++ ){   
        manufactured_Year.push(cars_inventory[car_index].car_year);
    }
    return manufactured_Year;
}